﻿namespace Homework13.Data
{
    public class MyType
    {
        public float MyFloatProperty { get; set; }

        public override string ToString()
        {
            return $"MyType {{MyFloatProperty: {MyFloatProperty} }}";
        }
    }
}
