﻿using System.Collections.Generic;

namespace Homework13.Data
{
    public class MyTypeRepo
    {
        public IEnumerable<MyType> GetAll
        {
            get
            {
                var types = new List<MyType>();
                types.Add(new MyType { MyFloatProperty = 10 });
                types.Add(new MyType { MyFloatProperty = 1 });
                types.Add(new MyType { MyFloatProperty = 12 });
                types.Add(new MyType { MyFloatProperty = 101 });
                types.Add(new MyType { MyFloatProperty = 22 });
                return types;
            }
        }
    }
}
