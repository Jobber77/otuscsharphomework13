﻿using System;
using System.Collections.Generic;

namespace Homework13.Extensions
{
    public static class EnumerableExtensions
    {
        public static T GetMax<T>(this IEnumerable<T> e, Func<T, float> getParametr) where T : class
        {
            if (e == null)
                throw new ArgumentNullException(nameof(e));

            if (getParametr == null)
                throw new ArgumentNullException(nameof(getParametr));

            T max = null;
            foreach (var item in e)
            {
                if (max == null)
                    max = item;

                if (getParametr(item) > getParametr(max))
                    max = item;
            }
            return max;
        }
    }
}
