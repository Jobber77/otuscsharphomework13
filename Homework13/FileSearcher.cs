﻿using Homework13.Events;
using System;
using System.IO;
using System.Threading;

namespace Homework13
{
    public class FileSearcher
    {
        public event EventHandler<FileArgs> OnFileFound;

        private CancellationTokenSource _cancelTokenSource;
        private CancellationToken _token;

        public void CancelSearch()
        {
            _cancelTokenSource.Cancel();
            OnFileFound = null;
        }

        public FileSearcher()
        {
            _cancelTokenSource = new CancellationTokenSource();
            _token = _cancelTokenSource.Token;
        }

        public void StartSearch(string fullFolderPath)
        {
            if (!Directory.Exists(fullFolderPath))
                throw new DirectoryNotFoundException(fullFolderPath);

            var directory = new DirectoryInfo(fullFolderPath);
            foreach (var file in directory.GetFiles())
            {
                if (_token.IsCancellationRequested)
                    break;
                OnFileFound?.Invoke(this, new FileArgs { FileName = file.Name });
            };
            Console.WriteLine("Search complete");
        }
    }
}
