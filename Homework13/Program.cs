﻿using Homework13.Data;
using Homework13.Extensions;
using System;
using System.IO;

namespace Homework13
{
    class Program
    {
        private static string STOP_FILE_NAME = "stop.txt";

        static void Main(string[] args)
        {
            var repo = new MyTypeRepo();
            var data = repo.GetAll;
            Console.WriteLine("Max element in data collection:");
            var max = data.GetMax<MyType>(item => item.MyFloatProperty);
            Console.WriteLine(max);

            Console.WriteLine("Input directory for search. Full path required.");
            var searcher = new FileSearcher();
            searcher.OnFileFound += (searcher, args) =>
            {
                Console.WriteLine($"File Found: {args.FileName}");
                if (args.FileName == STOP_FILE_NAME)
                    ((FileSearcher)searcher).CancelSearch();
            };
            try
            {
                searcher.StartSearch(Console.ReadLine());
            }
            catch (DirectoryNotFoundException ex)
            {
                Console.WriteLine("Such directory not exists");
            }
            Console.ReadLine();
        }
    }
}
